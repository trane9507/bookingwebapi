package com.edu.bookingwebapi.repository;

import com.edu.bookingwebapi.entity.Booking;
import com.edu.bookingwebapi.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    List<Booking> findBookingsByUser(User user);
}
