package com.edu.bookingwebapi.service;

import com.edu.bookingwebapi.entity.Booking;
import com.edu.bookingwebapi.entity.Room;
import com.edu.bookingwebapi.entity.User;
import com.edu.bookingwebapi.exception.room.RoomNotAvailableException;
import com.edu.bookingwebapi.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class BookingService {

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private RoomService roomService;

    public List<Booking> findBookingsByUser(User user) {
        return bookingRepository.findBookingsByUser(user);
    }

    public void save(Booking booking) throws RoomNotAvailableException {
        Collection<Room> rooms = booking.getRooms();
        for (Room room : rooms) {
            if (!roomService.isRoomAvailable(room, booking.getDateFrom(), booking.getDateTo()))
                throw new RoomNotAvailableException(String.format("Room number '%s' is not available", room.getNumber()));
        }

        bookingRepository.save(booking);
    }
}
