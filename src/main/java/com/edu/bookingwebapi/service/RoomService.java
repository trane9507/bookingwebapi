package com.edu.bookingwebapi.service;

import com.edu.bookingwebapi.entity.Booking;
import com.edu.bookingwebapi.entity.Room;
import com.edu.bookingwebapi.exception.room.InvalidRoomException;
import com.edu.bookingwebapi.exception.room.NoSuchRoomException;
import com.edu.bookingwebapi.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class RoomService {

    @Autowired
    private RoomRepository roomRepository;

    public List<Room> findAll() {
        return roomRepository.findAll();
    }

    public void save(Room room) throws InvalidRoomException {
        roomRepository.save(room);
    }

    public List<Room> getRoomsById(List<Long> roomsId) {
        List<Room> rooms = new ArrayList<>();
        for (Long id : roomsId) {
            Optional<Room> room = roomRepository.findById(id);
            if (room.isEmpty())
                throw new NoSuchRoomException(String.format("Room number %s not found", id));

            rooms.add(room.get());
        }
        return rooms;
    }

    public List<Room> getAvailableRooms(LocalDateTime dateFrom, LocalDateTime dateTo) {
        List<Room> availableRooms = new ArrayList<>();
        List<Room> rooms = findAll();
        for (Room room : rooms) {
            if (isRoomAvailable(room, dateFrom, dateTo))
                availableRooms.add(room);
        }

        return availableRooms;
    }

    public boolean isRoomAvailable(Room room, LocalDateTime dateFrom, LocalDateTime dateTo) {
        Collection<Booking> bookings = room.getBookings();
        for (Booking b : bookings) {
            long secondsBookingFrom2From = Duration.between(b.getDateFrom(), dateFrom).toSeconds();
            long secondsBookingFrom2To = Duration.between(b.getDateFrom(), dateTo).toSeconds();
            long secondsFrom2BookingTo = Duration.between(dateFrom, b.getDateTo()).toSeconds();
            long secondsTo2BookingTo = Duration.between(dateTo, b.getDateTo()).toSeconds();

            if (dateFrom.isBefore(b.getDateFrom()) && dateTo.isAfter(b.getDateTo())
                || dateFrom.isAfter(b.getDateFrom()) && dateTo.isBefore(b.getDateTo())
                || secondsBookingFrom2From >= 0 && secondsFrom2BookingTo >= 0
                || secondsTo2BookingTo >= 0 && secondsBookingFrom2To >= 0
                )
                return false;
        }
        return true;
    }
}
