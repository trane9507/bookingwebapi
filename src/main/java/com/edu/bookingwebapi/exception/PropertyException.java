package com.edu.bookingwebapi.exception;

import java.io.IOException;

public class PropertyException extends IOException {
    public PropertyException(String message) {
        super(message);
    }
}
