package com.edu.bookingwebapi.exception;

public class InvalidDateTimeException extends Exception {
    public InvalidDateTimeException(String message) {
        super(message);
    }
}
