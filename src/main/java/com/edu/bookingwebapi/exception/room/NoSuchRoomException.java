package com.edu.bookingwebapi.exception.room;

import java.util.NoSuchElementException;

public class NoSuchRoomException extends NoSuchElementException {
    public NoSuchRoomException(String message) {
        super(message);
    }
}
