package com.edu.bookingwebapi.exception.room;

public class InvalidRoomException extends Exception {
    public InvalidRoomException(String message) {
        super(message);
    }
}
