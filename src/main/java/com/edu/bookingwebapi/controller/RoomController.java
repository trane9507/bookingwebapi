package com.edu.bookingwebapi.controller;

import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.entity.Room;
import com.edu.bookingwebapi.exception.InvalidDateTimeException;
import com.edu.bookingwebapi.exception.room.InvalidRoomException;
import com.edu.bookingwebapi.service.RoomService;
import com.edu.bookingwebapi.validator.booking.BookingIntervalValidator;
import com.edu.bookingwebapi.validator.datetime.EmptyDateTimeValidator;
import com.edu.bookingwebapi.validator.datetime.ParseDateTimeValidator;
import com.edu.bookingwebapi.validator.room.RoomValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.hibernate.exception.ConstraintViolationException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/rooms")
@Slf4j
public class RoomController {

    @Autowired
    private RoomService roomService;

    @Autowired
    private EmptyDateTimeValidator emptyDateTimeValidator;
    @Autowired
    private ParseDateTimeValidator parseDateTimeValidator;
    @Autowired
    private BookingIntervalValidator bookingIntervalValidator;
    @Autowired
    private RoomValidator roomValidator;

    @GetMapping
    public String getAllRooms(Map<String, Object> model) {
        List<Room> rooms = roomService.findAll();
        model.put("rooms", rooms);
        log.debug("Successful get request for /rooms");
        return "room";
    }

    @PostMapping("/add")
    public String addRoom(@RequestParam Integer number, Map<String, Object> model) {
        Room room = new Room();
        room.setNumber(number);

        ValidationResult validationResult = roomValidator.validate(room);

        if (!validationResult.isValid()) {
            log.warn(validationResult.toString());
            model.put("message", validationResult.toString());
            return "room";
        }

        try {
            roomService.save(room);
            log.info("A new room number " + room.getNumber() + " has been created");
        } catch (InvalidRoomException e) {
            log.warn(e.toString());
            model.put("message", e.getMessage());
            return "room";
        } catch (DataIntegrityViolationException e) {
            log.warn(e.toString());
            model.put("message", "Room`s numbers must be unique");
            return "room";
        }

        log.debug("Successful post request for /rooms/add");
        return "redirect:/rooms";
    }

    @PostMapping("/available")
    public String getAvailableRooms(@RequestParam("date_from") String dateFromString,
                                    @RequestParam("date_to") String dateToString,
                                    Map<String, Object> model) {
        try {
            emptyDateTimeValidator.validate(dateFromString, dateToString);
            log.debug("EmptyDateTimeValidation was successful");
        } catch (InvalidDateTimeException e) {
            log.warn(e.toString());
            model.put("message", e.getMessage());
            return "room";
        }

        LocalDateTime dateFrom;
        LocalDateTime dateTo;
        try {
            dateFrom = parseDateTimeValidator.tryParseDateTime(dateFromString);
            dateTo = parseDateTimeValidator.tryParseDateTime(dateToString);
            log.debug("Parse datetime was successful");
        } catch (InvalidDateTimeException e) {
            log.warn(e.toString());
            model.put("message", e.getMessage());
            return "room";
        }

        ValidationResult validationResult = bookingIntervalValidator.validate(dateFrom, dateTo);
        if (!validationResult.isValid()) {
            log.warn(validationResult.toString());
            model.put("message", validationResult.toString());
            return "room";
        }

        List<Room> rooms = roomService.getAvailableRooms(dateFrom, dateTo);
        model.put("rooms", rooms);

        log.debug("Successful post request for /rooms/available");
        return "room";
    }
}
