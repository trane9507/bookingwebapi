package com.edu.bookingwebapi.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
@Slf4j
public class MainController {

    @GetMapping("/")
    public String main(Principal principal) {
        if (principal != null)
            return "redirect:/rooms";

        log.debug("Successful get request for /");
        return "main";
    }
}
