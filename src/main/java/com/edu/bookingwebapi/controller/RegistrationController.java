package com.edu.bookingwebapi.controller;

import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.entity.Role;
import com.edu.bookingwebapi.entity.User;
import com.edu.bookingwebapi.repository.UserRepository;
import com.edu.bookingwebapi.validator.user.UserValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.Map;

@Controller
@RequestMapping("/registration")
@Slf4j
public class RegistrationController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserValidator userValidator;

    @GetMapping()
    public String registration(@ModelAttribute("user") User user) {
        log.debug("Successful get request for /registration");
        return "registration";
    }

    @PostMapping()
    public String addUser(@ModelAttribute("user") User user, Map<String, Object> model) {
        ValidationResult validationResult = userValidator.validate(user);

        if (!validationResult.isValid()) {
            log.warn(validationResult.toString());
            model.put("message", validationResult.toString());
            return "registration";
        }

        User userFromDb = userRepository.findUserByUsername(user.getUsername());

        if (userFromDb != null) {
            log.info("A user named " + userFromDb.getUsername() + " already exists");
            model.put("message", "User named " + userFromDb.getUsername() + " already exists");
            return "registration";
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(Collections.singleton(Role.USER));

        userRepository.save(user);
        log.info("A new user named " + user.getUsername() + " has been created");

        log.debug("Successful post request for /registration");
        return "redirect:/login";
    }
}
