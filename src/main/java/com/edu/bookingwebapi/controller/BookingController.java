package com.edu.bookingwebapi.controller;

import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.entity.Booking;
import com.edu.bookingwebapi.entity.User;
import com.edu.bookingwebapi.exception.InvalidDateTimeException;
import com.edu.bookingwebapi.exception.room.NoSuchRoomException;
import com.edu.bookingwebapi.exception.room.RoomNotAvailableException;
import com.edu.bookingwebapi.service.BookingService;
import com.edu.bookingwebapi.service.RoomService;
import com.edu.bookingwebapi.service.UserService;
import com.edu.bookingwebapi.validator.booking.BookingValidator;
import com.edu.bookingwebapi.validator.datetime.EmptyDateTimeValidator;
import com.edu.bookingwebapi.validator.datetime.ParseDateTimeValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/bookings")
@Slf4j
public class BookingController {

    @Autowired
    private BookingService bookingService;
    @Autowired
    private UserService userService;
    @Autowired
    private RoomService roomService;

    @Autowired
    private BookingValidator bookingValidator;
    @Autowired
    private EmptyDateTimeValidator emptyDateTimeValidator;
    @Autowired
    private ParseDateTimeValidator parseDateTimeValidator;

    @GetMapping("/my")
    public String getBookingOfUser(Principal principal, Map<String, Object> model) {
        User user = userService.findByUsername(principal.getName());
        List<Booking> bookings = bookingService.findBookingsByUser(user);
        model.put("bookings", bookings);
        model.put("user", principal);
        log.debug("Successful get request for /bookings/my");
        return "booking";
    }

    @PostMapping("/add")
    public String addBooking(@RequestParam(value = "rooms", required = false) List<Long> roomsId,
                             @RequestParam String comment,
                             @RequestParam("date_from") String dateFrom,
                             @RequestParam("date_to") String dateTo,
                             Principal principal, Map<String, Object> model) {
        if (roomsId != null && roomsId.size() > 0) {
            log.debug("Room ids detected");

            Booking booking = new Booking();

            if (!comment.isBlank()) {
                booking.setComment(comment);
                log.debug("Comment is not blank");
            } else
                log.debug("Comment is blank");

            try {
                emptyDateTimeValidator.validate(dateFrom, dateTo);
                log.debug("EmptyDateTimeValidation was successful");

                booking.setDateFrom(parseDateTimeValidator.tryParseDateTime(dateFrom));
                booking.setDateTo(parseDateTimeValidator.tryParseDateTime(dateTo));
                log.debug("Parse datetime was successful");
            } catch (InvalidDateTimeException e) {
                log.warn(e.toString());
                model.put("message", e.getMessage());
                return "room";
            }

            booking.setUser(userService.findByUsername(principal.getName()));

            try {
                booking.setRooms(roomService.getRoomsById(roomsId));
                log.debug("All rooms detected");
            } catch (NoSuchRoomException e) {
                log.warn(e.toString());
                model.put("message", e.getMessage());
                return "room";
            }

            ValidationResult validationResult = bookingValidator.validate(booking);

            if (!validationResult.isValid()) {
                log.info(validationResult.toString());
                model.put("message", validationResult.toString());
                return "room";
            }

            try {
                bookingService.save(booking);
                log.info("A new booking has been created");
            } catch (RoomNotAvailableException e) {
                log.warn(e.toString());
                model.put("message", e.getMessage());
                return "room";
            }
        } else {
            log.warn("Room ids are missing");
            model.put("message", "Room ids are missing");
        }
        log.debug("Successful post request for /bookings/add");
        return "room";
    }
}
