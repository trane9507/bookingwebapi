package com.edu.bookingwebapi.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "room")
@ToString(exclude = "bookings")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private Integer number;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "rooms")
    private Collection<Booking> bookings;
}
