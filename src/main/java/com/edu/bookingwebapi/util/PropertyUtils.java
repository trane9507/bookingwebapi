package com.edu.bookingwebapi.util;

import com.edu.bookingwebapi.exception.PropertyException;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

@Component
public class PropertyUtils {

    private final String defaultPathToProperty = "src/main/resources/application.properties";
    private String pathToProperty = defaultPathToProperty;

    public String getMessage(String path) throws PropertyException {
        File file = new File(pathToProperty);

        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            Properties properties = new Properties();
            properties.load(fileInputStream);
            return properties.getProperty(path);
        } catch (FileNotFoundException e) {
            throw new PropertyException("Property`s file was not found");
        } catch (IOException e) {
            throw new PropertyException("Property reading error");
        } finally {
            pathToProperty = defaultPathToProperty;
        }
    }

    public Integer getValue(String path) throws PropertyException {
        return Integer.valueOf(getMessage(path));
    }

    public PropertyUtils setPathToProperty(String pathToProperty) {
        this.pathToProperty = pathToProperty;
        return this;
    }
}
