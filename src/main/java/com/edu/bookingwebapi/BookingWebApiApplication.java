package com.edu.bookingwebapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookingWebApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(BookingWebApiApplication.class, args);
    }
}
