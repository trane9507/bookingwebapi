package com.edu.bookingwebapi.validator.booking;

import br.com.fluentvalidator.context.Error;
import br.com.fluentvalidator.context.ValidationResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class BookingIntervalValidator {

    @Value("${booking.interval.value}")
    private Integer minBookingIntervalValue;

    @Value("${date.from-is-before-now}")
    private String isFromBeforeNowMessage;

    @Value("${date.from-is-after-to}")
    private String isFromAfterToMessage;

    @Value("${date.interval.message}")
    private String minBookingIntervalMessage;

    public ValidationResult validate(LocalDateTime dateFrom, LocalDateTime dateTo) {
        Collection<Error> errors = new ArrayList<>();

        if (dateFrom.isAfter(dateTo))
            errors.add(Error.create(null, isFromAfterToMessage, null, null));

        if (dateFrom.isBefore(LocalDateTime.now()))
            errors.add(Error.create(null, isFromBeforeNowMessage, null, null));

        if (Duration.between(dateFrom, dateTo).toMinutes() < minBookingIntervalValue)
            errors.add(Error.create(null, minBookingIntervalMessage, null, null));

        if (errors.isEmpty())
            return ValidationResult.ok();

        return ValidationResult.fail(errors);
    }
}
