package com.edu.bookingwebapi.validator.booking;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.entity.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static br.com.fluentvalidator.predicate.LocalDateTimePredicate.localDateTimeAfterNow;
import static br.com.fluentvalidator.predicate.LocalDateTimePredicate.localDateTimeAfterOrEqual;

@Component
public class BookingValidator extends AbstractValidator<Booking> {

    @Value("${date.future}")
    private String isDateNotFuture;

    @Value("${date.future-or-present}")
    private String isDateNotFutureOrNow;

    @Autowired
    private BookingIntervalValidator bookingIntervalValidator;

    @Override
    public ValidationResult validate(Booking instance) {
        ValidationResult result = super.validate(instance);
        if (!result.isValid())
            return result;

        return bookingIntervalValidator.validate(instance.getDateFrom(), instance.getDateTo());
    }

    @Override
    public void rules() {
        ruleFor(Booking::getDateFrom)
                .must(localDateTimeAfterOrEqual(LocalDateTime.now()))
                    .withMessage(isDateNotFutureOrNow)
                    .withFieldName("dateFrom")
                    .critical();

        ruleFor(Booking::getDateTo)
                .must(localDateTimeAfterNow())
                    .withMessage(isDateNotFuture)
                    .withFieldName("dateTo")
                    .critical();
    }
}
