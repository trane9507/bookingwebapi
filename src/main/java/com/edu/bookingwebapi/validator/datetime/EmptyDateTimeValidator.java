package com.edu.bookingwebapi.validator.datetime;

import com.edu.bookingwebapi.exception.InvalidDateTimeException;
import org.springframework.stereotype.Component;

@Component
public class EmptyDateTimeValidator {

    public void validate(String dateFrom, String dateTo) throws InvalidDateTimeException {
        if (dateFrom == null || dateFrom.isBlank())
            throw new InvalidDateTimeException("Invalid date to");

        if (dateTo == null || dateTo.isBlank())
            throw new InvalidDateTimeException("Invalid date from");
    }
}
