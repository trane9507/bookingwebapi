package com.edu.bookingwebapi.validator.datetime;

import com.edu.bookingwebapi.exception.InvalidDateTimeException;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

@Component
public class ParseDateTimeValidator {

    public LocalDateTime tryParseDateTime(String dateTime) throws InvalidDateTimeException {
        try {
            return LocalDateTime.parse(dateTime);
        } catch (DateTimeParseException e) {
            throw new InvalidDateTimeException("String cannot parse to datetime");
        }
    }
}
