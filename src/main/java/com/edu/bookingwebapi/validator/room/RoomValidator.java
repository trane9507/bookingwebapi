package com.edu.bookingwebapi.validator.room;

import br.com.fluentvalidator.AbstractValidator;
import com.edu.bookingwebapi.entity.Room;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static br.com.fluentvalidator.predicate.ComparablePredicate.greaterThan;
import static br.com.fluentvalidator.predicate.LogicalPredicate.not;
import static br.com.fluentvalidator.predicate.ObjectPredicate.nullValue;

@Component
public class RoomValidator extends AbstractValidator<Room> {

    @Value("${room.number.positive}")
    private String isNotPositiveNumberMessage;

    @Value("${room.number.null}")
    private String isNullNumberMessage;

    @Override
    public void rules() {
        ruleFor(Room::getNumber)
                .must(not(nullValue()))
                    .withMessage(isNullNumberMessage)
                    .withFieldName("number")
                    .critical()
                .must(greaterThan(0))
                    .withMessage(isNotPositiveNumberMessage)
                    .withFieldName("number")
                    .critical();
    }
}
