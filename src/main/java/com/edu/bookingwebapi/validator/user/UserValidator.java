package com.edu.bookingwebapi.validator.user;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.context.Error;
import br.com.fluentvalidator.context.ValidationResult;
import com.edu.bookingwebapi.entity.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

import static br.com.fluentvalidator.predicate.LogicalPredicate.not;
import static br.com.fluentvalidator.predicate.StringPredicate.stringEmptyOrNull;

@Component
public class UserValidator extends AbstractValidator<User> {

    @Value("${field.empty}")
    private String isEmptyOrNullFieldMessage;

    @Value("${user.name.size.message}")
    private String isIncorrectSizeUsernameMessage;
    @Value("${user.name.size.min.value}")
    private Integer minSizeUsername;
    @Value("${user.name.size.max.value}")
    private Integer maxSizeUsername;

    @Value("${user.password.size.message}")
    private String isIncorrectSizePasswordMessage;
    @Value("${user.password.size.min.value}")
    private Integer minSizePassword;
    @Value("${user.password.size.max.value}")
    private Integer maxSizePassword;

    @Override
    public ValidationResult validate(User instance) {
        ValidationResult result = super.validate(instance);
        if (!result.isValid())
            return result;

        Collection<Error> errors = new ArrayList<>();

        String username = instance.getUsername();
        if (minSizeUsername > username.length() || username.length() > maxSizeUsername)
            errors.add(Error.create("username", isIncorrectSizeUsernameMessage, null, username.length()));

        String password = instance.getPassword();
        if (minSizePassword > password.length() || password.length() > maxSizePassword)
            errors.add(Error.create("password", isIncorrectSizePasswordMessage, null, password.length()));

        if (errors.isEmpty())
            return ValidationResult.ok();

        return ValidationResult.fail(errors);
    }

    @Override
    public void rules() {
        ruleFor(User::getUsername)
                .must(not(stringEmptyOrNull()))
                    .withMessage(isEmptyOrNullFieldMessage)
                    .withFieldName("username")
                    .critical();

        ruleFor(User::getPassword)
                .must(not(stringEmptyOrNull()))
                    .withMessage(isEmptyOrNullFieldMessage)
                    .withFieldName("password")
                    .critical();
    }
}
